<?php

declare(strict_types=1);

namespace Emersoft\F2BanBundle\Security\Authenticator;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authentication\DefaultAuthenticationFailureHandler;
use Symfony\Component\Security\Http\HttpUtils;

class FailureHandler extends DefaultAuthenticationFailureHandler
{

    public function __construct(HttpKernelInterface $httpKernel, HttpUtils $httpUtils, array $options = [], LoggerInterface $logger = null)
    {
        parent::__construct($httpKernel, $httpUtils, $options, $logger);
    }

    /**
     * onAuthenticationFailure
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $this->logger->info('Authentication failed for IP: ' . $this->getClientIpFromRequest($request));

        return parent::onAuthenticationFailure($request, $exception);
    }

    private function getClientIpFromRequest(Request $request): ?string
    {
        if ($request->headers->has('CF-Connecting-IP')) {
            $cfConnectingIps = explode(',', $request->headers->get('CF-Connecting-IP', ''));

            if (isset($cfConnectingIps[0])) {
                return (string) array_pop($cfConnectingIps);
            }
        }
        return $request->getClientIp();
    }

}
