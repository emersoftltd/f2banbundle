# Fail2BanBundle

## 1. Configuration

### 1.1. Define monolog handler
File: `config/packages/monolog.yaml`

```yaml
monolog:
    channels: ['f2b']
    handlers:
        f2b:
            type: stream
            path: "%env(resolve:F2BAN_LOG_FILE)%"
            level: info
            channels: [ "f2b" ]
```

### 1.2. Update firewall configuration
File: `config/packages/security.yaml`

```yaml
security:
    # ...
    
    firewalls:
        admin:
            pattern: ^/admin
            form_login:
                failure_handler: Emersoft\F2BanBundle\Security\Authenticator\FailureHandler
        
        api:
            pattern: ^/api
            stateless: true
            json_login:
                failure_handler: Emersoft\F2BanBundle\Security\Authenticator\FailureHandler
```